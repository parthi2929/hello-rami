# Hello Rami

Creating a hello world tutorial project for [RAMI 4.0](https://www.plattform-i40.de/IP/Redaktion/EN/Downloads/Publikation/rami40-an-introduction.pdf?__blob=publicationFile&v=7) 

Hopefully this could be helpful in `Work,education and training` part of  [I4.0 Structure](https://www.plattform-i40.de/IP/Redaktion/EN/Bilder/graphic-plattform-4-0.html). 


![I4.0 Structure](docs/content/assets/i4str.jpg) 


**For Editors:**  
* if you use vscode or similar editor, I propose a editor + live preview setup as below, as vscode's markdown preview does not do justice for this rich documentation creation, esp failing image references for markdown preview leading to hacks. [1](https://discourse.gohugo.io/t/replace-image-reference-link/35314/6)

![tip-01](docs/static/tip-01.jpg)