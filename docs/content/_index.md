---
title: "Hello RAMI Projects"
---

# Hello RAMI

Creating a hello world tutorial project for [RAMI 4.0](https://www.plattform-i40.de/IP/Redaktion/EN/Downloads/Publikation/rami40-an-introduction.pdf?__blob=publicationFile&v=7) 

Hopefully this could be helpful in `Work,education and training` part of  [I4.0 Structure](https://www.plattform-i40.de/IP/Redaktion/EN/Bilder/graphic-plattform-4-0.html). 

![i4structure](/i4str.jpg)


