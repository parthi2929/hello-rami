---
title: Credits
disableToc: true
---

## Contributors

* [Christoph Binder](https://www.linkedin.com/in/christoph-binder-1a2383178/) - the guy to go for standards. Immensely helpful and supportive in this venture. 


## Reference

* [Archimate 5 minute guide](https://www.youtube.com/watch?v=PXr5gVGlbNU&list=PLUxDCM4ujDqXDI2P2Vm2Ruoj6VfQVJRvv) - simple easy to understand Archimate tutorials. I heavily borrow and derive my content from this tutorial. 
* [Archimate Specification](https://pubs.opengroup.org/architecture/archimate31-doc/toc.html) - some ardent archimate lover may find our liberal playful usage offensive, so I sometimes also try to refer to this bible. 

## Packages and libraries
* [Hugo Learn Theme](https://github.com/matcornic/hugo-theme-learn) - One of few themese supporting multi level menu, very important core requirement for me for these documentations. 

## Tooling

* [Netlify](https://www.netlify.com) - For CI and deploying this documentation
* [Hugo](https://gohugo.io/) - the base documentation framework this site uses.