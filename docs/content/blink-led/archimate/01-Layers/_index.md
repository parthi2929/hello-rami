+++
title = "1. Layers"
description = "This is a demo child page"
+++


Archimate Framework has 3 core layers. Business, Application and Technology. These are represented typically by colors as below. 

![](/archimate/core-layers.jpg)

There are also icons associated and relationships between these entities. In above example, a _technology_ entity _serves_ an _application_ entity, which in turn _serves_ a _business_ entity. 

There can be various icons associated with these entities, representing more information about them. For example, a _technology_ can simply be a _device_ or a _node_.  

Instead of including an icon to rectangular elements, it is also typical to represnt the entity in icon shape as well. 

### Blink-LED Project

In our case, we could thus establish a very high level archimate core model as below. 

![MainView](/archimate//MainView.jpg)

At the core of our system, its a simple blinking LED, with a coded frequency. Our purpose is to understand modelling a system using Archimate, so I introduce redundant additional information, just to understand the modelling concepts. So as we go, we would be making up of stories to understand archimate modelling. 


#### LED On/Off Service Desk
That said, in our case, we could say, we provide a business service to the world. What service? A blinking LED. Simply we could image a petty shop, showcasing only _that_ service to whoever customer approaches us. They might ask for a blinking service(?!), and we (business actor) may switch that on for a fixed period, and charge the customer. 

#### LED On/Off Application 
How does our business actor (providing the service) do that? Using our Blinking LED _application_. He/She may, start, say a windows application or directly uses the hardware to press a button to start blinking service. 

#### LED Device
How does that software/hardware application translate to reality? Finally the application resides or controls an actual hardware, which we could call, a LED device or a system, which then gives a response as per those inputs. The technology layer simply represents these devices and their properties. 

#### But Why?
The example may look trivial at this highest bird view of models upcoming. But we have a reason here. This high level view could have been explained with a more complicated system, like `Appointment Booking System` where multiple actors, applications, devices are involved. That is what many traditional tutorials do, when trying to explain high level modelling. However, once we go down the layers, the complexity increases simply because of sheer number of requirements, design, architecture, process components involved. We wanted our tutorial to be end to end, **and** at same time simple enough at even down the layers to follow easily. This is why we take with simplest technical example, and build up stories to create models out of it. This way, the technical complexity can be kept out of our way to an extent. 

