+++
title = "3. Relationships"
description = "This is a demo child page"
+++

Relationships or Connectors define interactions between entities or objects. 

The relationships are classified as follows.

* `Structural relationships`, which model the static construction or composition of concepts of the same or different types
* `Dependency relationships`, which model how elements are used to support other elements
* `Dynamic relationships`, which are used to model behavioral dependencies between elements
* `Other relationships`, which do not fall into one of the above categories

We already saw relationship in main view. 

![](/archimate/MainView.jpg)

It is a good practice to explicitly also annotate them so as to avoid ambiguity. 

![](/archimate/main-view-01.jpg)

Below are the sub relationship type under each relationship category, including relevant connector icon to use. 

![](/archimate/relationships-01.jpg)

For our purpose, we could liberally use them, as these are easily inferrable to an extent. However, an ardent compliant archimate architect may find this uncomfortable. But this is ok. There could be semantic missteps here and there, and we shall keep playing (like a 5th grader), and over time, these will get corrected (like how we learn English or anything from beginning). Also you have `Magic Connector` as saviour, if you use open source [archimate tool](https://www.archimatetool.com/) for modelling. 

With this spirit(?!), we could include technology aspect also to our main view, to seal our understanding. We already used _Serving_ relationship, and here, any other we find to be fit enough. I find `Trigger` relationship fits. I also renamed LED device to Arduino to suit here. 

![mv02](/archimate/main-view-02.jpg)

So, the Arduino device _triggers_ a control switch (on it), which in turn _triggers_ an LED device to switch on or off. Of course this step might be too basic, and redundant to state in a model, but only there to explain how we could make use of _relationships_. 

This is only a `Hello Archimate` tutorial, so we could stop with relationships here and move on to next topic. We will add more details as we find necessary later. For those interested a little more can enjoy [this](https://youtu.be/sEqzQcjPCAw) tutorial. 