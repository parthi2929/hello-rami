+++
title = "2. Aspects"
description = "This is a demo child page"
+++

An entity in any of the layers, has some capabilities. Aspects characteriszes those capabilities, provides constructs to explain them. The aspects can be of 3 types. 

* Active Structure,AS  (like a Subject)
* Behaviour, B (like a Verb)
* Passive Structure, PS  (like an Object)

| AS | B | PS |
|---|---|---|
| The Arduino | controls | the LED |

Note, all the above entities can be in a Technology layer. [This](https://youtu.be/-Zd7q9OcN6c?t=207) part of video tutorial has some more good examples for other layers. 

![aspects](/archimate/aspects-01.jpg)

{{% notice note %}}
Archimate Framework (AF) is only a guideline, not a hard and fast rule. So we apply these as needed in practice and here, reduntantly more to understand them. And there are always multiple ways to model same system in different levels of details and representations.  
{{% /notice %}}
