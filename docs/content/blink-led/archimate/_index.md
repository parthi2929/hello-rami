---
title: 1. Blink LED (Archimate)
weight: 20
---


As Archimate is the only open source modelling platform available, we start this first step with a simple example of, say a blinking LED with archimate modelling. 

## Core Framework 

The ArchiMate Core Framework is a framework of nine cells used to classify elements of the ArchiMate core language. It is made up of three aspects and three layers, as illustrated below. ([ref](https://pubs.opengroup.org/architecture/archimate31-doc/chap03.html#_Toc10045293))

![core-fw](/archimate/am-core-framework.jpg)

Of course, there are more layers as defined in [Archimate Full Framework](https://pubs.opengroup.org/architecture/archimate31-doc/chap03.html#_Toc10045294), which one can pounce upon after exploring core building blocks. 



