---
title: Blink LED
weight: 5
pre: "<b>1. </b>"
# chapter: true
---

We start with a very simple Blink LED project. One example, and multiple flavors. 

As we try to use FOSS tools as much as possible, starting with Archimate for modelling, the first flavor could be that with a simple LED example and next could be same, transformed to be compatable with RAMI. 

![flavors-01](/flavors-01-small.svg)
